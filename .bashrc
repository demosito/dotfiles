# ~/.bashrc: executed by bash(1) for non-login shells.
# see /usr/share/doc/bash/examples/startup-files (in the package bash-doc)
# for examples

# If not running interactively, don't do anything
case $- in
    *i*) ;;
      *) return;;
esac

# don't put duplicate lines or lines starting with space in the history.
# See bash(1) for more options
HISTCONTROL=ignoreboth

# append to the history file, don't overwrite it
shopt -s histappend

# for setting history length see HISTSIZE and HISTFILESIZE in bash(1)
HISTSIZE=5000
HISTFILESIZE=5000

# check the window size after each command and, if necessary,
# update the values of LINES and COLUMNS.
shopt -s checkwinsize

# If set, the pattern "**" used in a pathname expansion context will
# match all files and zero or more directories and subdirectories.
#shopt -s globstar

# make less more friendly for non-text input files, see lesspipe(1)
[ -x /usr/bin/lesspipe ] && eval "$(SHELL=/bin/sh lesspipe)"

# set variable identifying the chroot you work in (used in the prompt below)
if [ -z "${debian_chroot:-}" ] && [ -r /etc/debian_chroot ]; then
    debian_chroot=$(cat /etc/debian_chroot)
fi

# set a fancy prompt (non-color, unless we know we "want" color)
case "$TERM" in
    xterm-color|*-256color) color_prompt=yes;;
esac

# uncomment for a colored prompt, if the terminal has the capability; turned
# off by default to not distract the user: the focus in a terminal window
# should be on the output of commands, not on the prompt
#force_color_prompt=yes

if [ -n "$force_color_prompt" ]; then
    if [ -x /usr/bin/tput ] && tput setaf 1 >&/dev/null; then
	# We have color support; assume it's compliant with Ecma-48
	# (ISO/IEC-6429). (Lack of such support is extremely rare, and such
	# a case would tend to support setf rather than setaf.)
	color_prompt=yes
    else
	color_prompt=
    fi
fi

if [ "$color_prompt" = yes ]; then
    PS1='${debian_chroot:+($debian_chroot)}\[\033[01;32m\]\u@\h\[\033[00m\]:\[\033[01;34m\]\w\[\033[00m\]\$ '
else
    PS1='${debian_chroot:+($debian_chroot)}\u@\h:\w\$ '
fi
unset color_prompt force_color_prompt

# If this is an xterm set the title to user@host:dir
case "$TERM" in
xterm*|rxvt*)
    PS1="\[\e]0;${debian_chroot:+($debian_chroot)}\u@\h: \w\a\]$PS1"
    ;;
*)
    ;;
esac

# enable color support of ls and also add handy aliases
if [ -x /usr/bin/dircolors ]; then
    test -r ~/.dircolors && eval "$(dircolors -b ~/.dircolors)" || eval "$(dircolors -b)"
    alias ls='ls --color=auto'
    #alias dir='dir --color=auto'
    #alias vdir='vdir --color=auto'

    alias grep='grep --color=auto'
    alias fgrep='fgrep --color=auto'
    alias egrep='egrep --color=auto'
fi

# colored GCC warnings and errors
#export GCC_COLORS='error=01;31:warning=01;35:note=01;36:caret=01;32:locus=01:quote=01'

# some more ls aliases
alias ll='ls -alF'
alias la='ls -A'
alias l='ls -CF'

# Add an "alert" alias for long running commands.  Use like so:
#   sleep 10; alert
alias alert='notify-send --urgency=low -i "$([ $? = 0 ] && echo terminal || echo error)" "$(history|tail -n1|sed -e '\''s/^\s*[0-9]\+\s*//;s/[;&|]\s*alert$//'\'')"'

# Alias definitions.
# You may want to put all your additions into a separate file like
# ~/.bash_aliases, instead of adding them here directly.
# See /usr/share/doc/bash-doc/examples in the bash-doc package.

if [ -f ~/.bash_aliases ]; then
    . ~/.bash_aliases
fi

# enable programmable completion features (you don't need to enable
# this, if it's already enabled in /etc/bash.bashrc and /etc/profile
# sources /etc/bash.bashrc).
if ! shopt -oq posix; then
  if [ -f /usr/share/bash-completion/bash_completion ]; then
    . /usr/share/bash-completion/bash_completion
  elif [ -f /etc/bash_completion ]; then
    . /etc/bash_completion
  fi
fi

[ -f $HOME/.sshrc ] && . $HOME/.sshrc

# Predictable SSH authentication socket location.
#SOCK="/tmp/ssh-agent-$USER-tmux"
#AGENT_PID=$(/usr/bin/pgrep -u $USER ssh-agent)
#if [[ "$?" != "0" || "$AGENT_PID" == "" ]]; then # agent not running - start it
    #eval $(/usr/bin/ssh-agent -s -t 8h)
    #/usr/bin/ln -sf $SSH_AUTH_SOCK $SOCK
    #export SSH_AUTH_SOCK=$SOCK
#else
    #if [ ! -e $SOCK ]; then # we don't have valid socket - find it and link it
        #SOCK_DIR=$(/usr/bin/find /tmp/ -maxdepth 1 -type d -name "ssh-*" -user $USER | /usr/bin/head -n 1)
        #if [ -n "$SOCK_DIR" ]; then
            #/usr/bin/ln -sf "$SOCK_DIR/agent.$AGENT_PID" $SOCK
        #fi
    #fi
    #export SSH_AUTH_SOCK=$SOCK
    #export SSH_AGENT_PID=$AGENT_PID
#fi

afuse_mountpoint=$HOME/sshfs
afuse-start() {
    /usr/bin/afuse -o mount_template="sshfs %r:/ %m" \
                   -o unmount_template="fusermount -u -z %m" $afuse_mountpoint
}

# unmount all afuse sshfs mounts but leave afuse itself running
afuse-umount() {
    /usr/bin/fusermount -u -z $afuse_mountpoint
}

# umount all afuse mounts and terminate afuse itself
afuse-stop() {
    afuse-umount
    /usr/bin/pkill afuse
}

# df for "real" filesystems
ldf() {
    df -T -h -t xfs -t ext4 -t ext3 -t ext2 -t btrfs -t zfs -t exfat -t ntfs \
       -t jfs2

}

export EDITOR=vim

[ -f ~/.fzf.bash ] && source ~/.fzf.bash

alias bc='bc -lq'
alias gss='git status --short --branch'
alias glg="git log --graph --abbrev-commit --decorate --format=format:'%C(bold blue)%h%C(reset) - %C(bold green)(%ar)%C(reset) %C(white)%s%C(reset) %C(dim white)- %an%C(reset)%C(bold yellow)%d%C(reset)' --all"
alias gcm='git checkout master'
alias gco='git checkout'
alias gspu='git stash push'
alias gspo='git stash pop'
alias gsls='git stash list'
alias gsso='git stash show -p'
alias gcom='git commit -m'
alias ga='git add -p'
alias gdi='git diff'
alias gpu='git pull'
alias gpush='git push'

alias te='terraform'

complete -C /usr/local/bin/terraform terraform

# decode a single JWT token
function jwt_decode() {
  for part in 1 2; do
    b64="$(cut -f$part -d. <<< "$1" | tr '_-' '/+')"
    len=${#b64}
    n=$((len % 4))
    if [[ 2 -eq n ]]; then
      b64="${b64}=="
    elif [[ 3 -eq n ]]; then
      b64="${b64}="
    fi
    d="$(openssl enc -base64 -d -A <<< "$b64")"
    echo "$d"
    # don't decode further if this is an encrypted JWT (JWE)
    if [[ 1 -eq part ]] && grep '"enc":' <<< "$d" >/dev/null ; then
        exit 0
    fi
  done
}

# decode a file with either a single plain JWT token or with JSON like
# { "id_token": "<id_token>", "access_token": "<access_token>", ... }
function jwt() {
    local in; read in;
    if echo "$in" | head -1 | grep "^{" &>/dev/null; then
        # input has several tokens in JSON
        local id_token=$(jq -r .id_token <<<"$in")
        jwt_decode "$id_token" | jq .
    else
        # input is a single base64 token
        jwt_decode "$in" | jq .
    fi
}

mcd ()
{
    mkdir -p -- "$1" &&
       cd -P -- "$1"
}

PATH="/home/vagrant/.local/bin:$PATH"

# source /home/vagrant/.config/broot/launcher/bash/br
