# Set-ExecutionPolicy RemoteSigned

Set-ExecutionPolicy Bypass -Scope Process -Force; [System.Net.ServicePointManager]::SecurityProtocol = [System.Net.ServicePointManager]::SecurityProtocol -bor 3072; iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))

choco install -y virtualbox virtualbox-guest-additions-guest.install vagrant git putty totalcommander hackfont altdrag

git clone https://gitlab.com/demosito/dotfiles.git

cd dotfiles/.config/mybootstrap

vagrant plugin install vagrant-vbguest
vagrant plugin install vagrant-multi-putty
vagrant plugin install vagrant-disksize

vagrant up
