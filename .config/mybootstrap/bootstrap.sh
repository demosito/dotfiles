#!/usr/bin/env bash

set -e

_script_dir="$(dirname $0)"

dotfiles_tmp_dir="$HOME/dotfiles_tmp_EDRTFGYBUHK"
dotfiles_repo_url="https://gitlab.com/demosito/dotfiles"
dotfiles_branch=${DOTFILES_BRANCH:-master}


# helper to determine if a binary is available in PATH
have_in_path() {
    which $@ &>/dev/null
    return $?
}

cleanup() {
    rm -rf -- "$dotfiles_tmp_dir" &>/dev/null
}
trap cleanup EXIT ERR INT TERM

echo "=== Bootsrapping for user $(whoami)"

if have_in_path apt; then
    sudo apt update && sudo apt install -y ansible git
elif have_in_path yum; then
    sudo yum install -y ansible git
fi

# clone dotfiles to ~ unless it's already there
if [ ! -d "$HOME/.git" ]; then
    git clone -n "$dotfiles_repo_url" "$dotfiles_tmp_dir"
    mv "$dotfiles_tmp_dir/.git" "$HOME"
fi

pushd "$HOME"
git checkout "$dotfiles_branch" || {
    git config --global user.email "demosito@gmail.com"
    git config --global user.name "Alexander Sauta"
    git stash save "dotfiles checkout" && git checkout "$dotfiles_branch" && git pull;
}
popd

ansible-playbook -i 'default,' -e ansible_connection=local \
                 "$HOME/.config/mybootstrap/ansible/workstation.yml"

echo "Install vim plugins"
echo | echo | vim +PlugInstall "+CocInstall coc-python" +qall
echo | echo | vim "+CocInstall coc-python" +qall
