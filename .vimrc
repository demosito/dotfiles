set nocompatible

" font and screen size for gui in all OSes {{{
if has("gui_running")
    set lines=44
    set columns=95

    if has("gui_gtk2")
        set guifont=Luxi\ Mono\ 12
    elseif has("x11")
        " Also for GTK 1
        set guifont=*-lucidatypewriter-medium-r-normal-*-*-180-*-*-m-*-*
    elseif has("gui_win32")
        behave mswin
        set guifont=courier_new:h12:cRUSSIAN
    endif
else
    set mouse=""
endif
"}}}

" TODO This doesn't actually work in windows
set fileformat=unix
set fileformats=unix,dos

set laststatus=2

"set statusline=[r%{HGRev()}]
"set statusline=%<%f\ %h%m%r%{fugitive#statusline()}%=%-14.(%l,%c%V%)\ %P 

"language switching (see mappings for F12 below )
set keymap=russian-jcukenwin
set iminsert=0
set imsearch=0

" make ]] and [[ ignore spaces before "{" and "}"
"noremap [[ ?{<CR>w99[{
"noremap ][ /}<CR>b99]}
"noremap ]] j0[[%/{<CR>
"noremap [] k$][%?}<CR>

"folding
set nofoldenable " don't fold by default
set foldmethod=indent

set nowrap

filetype plugin on
set visualbell

" backup and undo
if exists("*mkdir")
    silent! call mkdir($HOME.'/.vim/tmp/undo', 'p')
else
    echo "Please create dir ~/.vim/tmp/undo"
endif
set directory=$HOME/.vim/tmp
set backupdir=$HOME/.vim/tmp
set undofile
set undodir=$HOME/.vim/tmp/undo

set showcmd
set nostartofline " don't move cursor to first column on Ctrl-D and alike

set autoindent  "enable autoindent
set copyindent
set smarttab
set shiftwidth=4
set softtabstop=2
set expandtab
set virtualedit=all " nice squares in block selection mode


set ignorecase " ignore case while searching
set smartcase " but consider it when have capital letters

" enable incremental search
set incsearch
set hlsearch

set hidden " Don't close buffer on buffer switch
set confirm " Ask when leaving unsaved file
set autoread " auto reload changed files
set shiftround " drop unused spaces
" set nowrapscan " stop search when reached the bottom
set wildmenu " show menu on command completion
set wildmode=longest:full,list:full " complete commands till longest common string w/menu
set wildignore+=*/.git/*,*/.hg/*,*/.svn/*,*.pyc,*.o
" make whitespaces visible when 'list' is set
set listchars=tab:→\ ,eol:↵,trail:·,extends:↷,precedes:↶
set iskeyword+=\-  " autocomplete names with hyphens

filetype off                   " required!

let g:EasyMotion_do_mapping = 0 " it maps a lot of crap

let g:ycm_python_binary_path = 'python'
" disable diagnostics
let g:ycm_show_diagnostics_ui = 0
let g:ycm_enable_diagnostic_highlighting = 0

set rtp+=~/.vim
call plug#begin('~/.vim/plugged')

" Make sure you use single quotes
" Navigation
Plug 'preservim/nerdtree'
Plug 'majutsushi/tagbar'
Plug 'junegunn/goyo.vim'
Plug 'haya14busa/incsearch.vim'
Plug 'easymotion/vim-easymotion'
Plug 'rking/ag.vim'
Plug 'andymass/vim-matchup'
" Plug 'junegunn/fzf'
Plug 'junegunn/fzf', { 'do': { -> fzf#install()  }  }
Plug 'junegunn/fzf.vim'

" Editing
Plug 'mattn/emmet-vim'
Plug 'tpope/vim-surround'
Plug 'tpope/vim-commentary'
Plug 'tpope/vim-repeat'
Plug 'junegunn/vim-easy-align'

" Look & feel
Plug 'itchyny/lightline.vim'
Plug 'maximbaz/lightline-ale'

" Colorschemes
Plug 'jnurmine/Zenburn'
Plug 'chriskempson/base16-vim'
Plug 'junegunn/seoul256.vim'
Plug 'arcticicestudio/nord-vim'

" Other colors
Plug 'junegunn/limelight.vim'
Plug 'vim-scripts/txt.vim' " basic default highlight

" Development
Plug 'w0rp/ale' " Asynchronous Lint Engine - all languages
Plug 'janko-m/vim-test' " Universal test runner - all languages
Plug 'samoshkin/vim-mergetool'
Plug 'neoclide/coc.nvim', {'branch': 'release'}
Plug 'hashivim/vim-terraform'

" VCS
Plug 'tpope/vim-fugitive'

" All of your Plugins must be added before the following line
call plug#end()              " required


let base16colorspace=256  " 256 colors for base16
if has('gui_running')
    colorscheme base16-eighties
else
    colorscheme seoul256
endif

" Show trailing whitespace and spaces before a tab:
match ExtraWhitespace /\s\+$\| \+\ze\t/

" hilight column 80
if exists("g:zenburn_high_Contrast") " variable doesn't matter, just check for zenburn
    hi ColorColumn guibg=#303030
    if &t_Co > 255
        hi ColorColumn ctermbg=236
    endif
endif
set colorcolumn=89
set textwidth=88

autocmd FileType make setlocal noexpandtab " don't expand tabs in Makefile
autocmd FileType vim setlocal foldmethod=marker
autocmd FileType yaml setlocal ts=2 sts=2 sw=2 expandtab
autocmd FileType python setlocal ts=4 sts=4 sw=4 foldmethod=indent ai ff=unix
autocmd BufRead,BufNewFile * setfiletype txt " basic hilight by default, txt.vim
autocmd FileType python setlocal listchars=tab:→\  list
autocmd FileType python let b:coc_root_patterns = ['.git', '.env']
autocmd FileType python nnoremap <leader>t oimport ipdb; ipdb.set_trace()<ESC>

" Diff
" Do not highlight changed line, highlight only changed text within a line
hi! DiffText term=NONE ctermfg=215 ctermbg=233 cterm=NONE guifg=#FFB86C guibg=#14141a gui=NONE
hi! link DiffChange NONE
hi! clear DiffChange


" --- Goyo ---
let g:goyo_width = 100
" Toggle Goyo
nmap <C-W>O :Goyo<CR>

function! s:goyo_enter()
  silent !tmux set status off
  silent !tmux list-panes -F '\#F' | grep -q Z || tmux resize-pane -Z
endfunction

function! s:goyo_leave()
  silent !tmux set status on
  silent !tmux list-panes -F '\#F' | grep -q Z && tmux resize-pane -Z
endfunction

autocmd! User GoyoEnter nested call <SID>goyo_enter()
autocmd! User GoyoLeave nested call <SID>goyo_leave()

" --- EasyMotion ---
map <Space> <Plug>(easymotion-s)
map g<Space> <Plug>(easymotion-overwin-f)

" --- YouCompleteMe ---
let g:ycm_extra_conf_globlist = [ '~/.ycm_extra_conf.py' ]

" --- incsearch.vim
"if exists('g:loaded_incsearch')
    map /  <Plug>(incsearch-forward)
    map ?  <Plug>(incsearch-backward)
    map g/ <Plug>(incsearch-stay)
"endif

" --- Limelight ---
let g:limelight_default_coefficient = 0.6
"let g:limelight_conceal_ctermfg = '236'
" Toggle
nnoremap <Leader>l :Limelight!!<cr>


" --- python-mode ---
let g:pymode_folding = 0
let g:pymode_lint = 0
let g:pymode_rope_goto_definition_cmd = 'e' " goto in current window

" --- VimWiki ---
let g:vimwiki_list = [{'path': '~/.vim/vimwiki', 'path_html': '~/.vim/vimwiki'}]

" - optional -
" auto close options when exiting insert mode
autocmd InsertLeave * if pumvisible() == 0|pclose|endif
set completeopt=menu,menuone,preview

" - ctags -
" add current directory's generated tags file to available tags
set tags+=./tags

let g:LargeFile = 10

" --- TagBar ---
let g:tagbar_width = 30
let g:tagbar_autofocus = 1

" --- NERDTree ---
let g:NERDTreeHighlightCursorline = 1
let g:NERDChristmasTree = 1
let g:NERDTreeIgnore = ['\~$', '\.o$']
let g:NERDTreeQuitOnOpen = 1

" --- ShowMarks
 let g:showmarks_include="abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ" 

 let g:SrcExpl_pluginList = [
         \ "__Tag_List__",
         \ "NERD_tree_1",
         \ "-MiniBufExplorer-",
         \ "Source_Explorer"
         \ ]


" --- Syntastic
"set statusline+=%#warningmsg#
"set statusline+=%{SyntasticStatuslineFlag()}
"set statusline+=%*

"let g:syntastic_always_populate_loc_list = 1
"let g:syntastic_auto_loc_list = 1
"let g:syntastic_check_on_open = 1
"let g:syntastic_check_on_wq = 0

" --- Ctrl-P
if executable('ag')
  " Use The Silver Searcher https://github.com/ggreer/the_silver_searcher
  set grepprg=ag\ --nogroup\ --nocolor
  " Use ag in CtrlP for listing files. Lightning fast, respects .gitignore
  " and .agignore. Ignores hidden files by default.
  let g:ctrlp_user_command = 'ag %s -l --nocolor -f -g ""'
else
  "ctrl+p ignore files in .gitignore
  let g:ctrlp_user_command = ['.git', 'cd %s && git ls-files . -co --exclude-standard', 'find %s -type f']
endif

" --- ALE
let g:ale_echo_msg_format = "%linter% [%severity%] %code: %%s"
let g:ale_python_pylint_change_directory = 0

" --- Lightline + lightline-ALE
let g:lightline = {}

let g:lightline.component = {
      \     'tagbar': '%{tagbar#currenttag("%s", "", "f")}',
      \     'fugitive': '%{fugitive#statusline()}',
      \     'coc': 'coc: %{coc#status()}'
      \ }

let g:lightline.component_expand = {
      \  'linter_checking': 'lightline#ale#checking',
      \  'linter_warnings': 'lightline#ale#warnings',
      \  'linter_errors': 'lightline#ale#errors',
      \  'linter_ok': 'lightline#ale#ok',
      \ }

let g:lightline.component_type = {
      \    'linter_checking': 'left',
      \    'linter_warnings': 'warning',
      \    'linter_errors': 'error',
      \    'linter_ok': 'left',
      \ }

let g:lightline.active = {
      \     'left': [
      \         [ 'paste' ],
      \         [ 'readonly', 'filename', 'modified' ],
      \         [ 'fugitive' ],
      \         [ 'tagbar' ]
      \     ],
      \     'right': [
      \         [ 'lineinfo' ],
      \         [ 'linter_checking', 'linter_errors', 'linter_warnings', 'linter_ok' ],
      \         [ 'coc' ]
      \     ]
      \ }

" --- Deoplete
let g:deoplete#enable_at_startup = 1

" --- Coc
" Use <c-space> to trigger completion.
inoremap <silent><expr> <c-space> coc#refresh()
" GoTo code navigation.
nmap <silent> gd <Plug>(coc-definition)
nmap <silent> gy <Plug>(coc-type-definition)
nmap <silent> gi <Plug>(coc-implementation)
nmap <silent> gr <Plug>(coc-references)
nmap <leader>r <Plug>(coc-rename)

" --- Lexima
let g:lexima_ctrlh_as_backspace = 1

" ***************************** Key Mappings *****************************
map <F2> :w<CR>
map <F3> :%s/\<ddc\>/dd/g
set pastetoggle=<F5> " toggle paste option when in insert or normal mode
noremap <F6> :copen<CR>
nnoremap <silent> <F8> :CtrlPBuffer<CR>
nnoremap <silent> <F9> :NERDTreeToggle<CR>
nnoremap <silent> <F11> :TagbarToggle<CR>
" language switching
imap <F12> 
cmap <F12> 

" generate ctags for current folder:
map <C-F12> :!ctags -R --c++-kinds=+p --fields=+iaS --extra=+q .<CR><CR>

map <C-n> :Buffers<CR>

map ]q :cnext<CR>
map [q :cprevious<CR>

map ]Q <Plug>(ale_next)
map [Q <Plug>(ale_previous)


" search for visually selected text
vnoremap // y/<C-R>"<CR>

" NERDTree keys are mapped this way:
let NERDTreeMapOpenSplit = "h"
let NERDTreeMapJumpNextSibling = "<C-K>"
let NERDTreeMapJumpPrevSibling = "<C-I>"
let NERDTreeMapHelp = "<F1>"

" Map Y to act like D and C, i.e. to yank until EOL, rather than act as yy,
" which is the default
map Y y$

" Map <C-L> (redraw screen) to also turn off search highlighting until the
" next search
nnoremap <C-@> :nohl<CR><C-L>

" easy window movements
nnoremap <C-J> <C-W>j
nnoremap <C-K> <C-W>k
nnoremap <C-L> <C-W>l
nnoremap <C-H> <C-W>h

" Start interactive EasyAlign in visual mode (e.g. vip<Enter>)
vmap gl <Plug>(EasyAlign)

" Start interactive EasyAlign for a motion/text object (e.g. gaip)
nmap gl <Plug>(EasyAlign)

" ZoomWin emulation with tab
map <C-W>o :tab split<CR>

" FZF
" git files
map <C-P> :Files<CR>
" tags in current buffer
map <leader>fg :GFiles<CR>
map <leader>ft :Tags<CR>
nnoremap <leader>fb :BTags<CR>
map <leader>fl :BLines<CR>
" command history
map <leader>fh :History:<CR>
" search history
map <leader>f/ :History/<CR>

" --- MergeTool ---
nmap <expr> <Left> &diff? '<Plug>(MergetoolDiffExchangeLeft)' : '<Left>'
nmap <expr> <Right> &diff? '<Plug>(MergetoolDiffExchangeRight)' : '<Right>'
nmap <expr> <Down> &diff? '<Plug>(MergetoolDiffExchangeDown)' : '<Down>'
nmap <expr> <Up> &diff? '<Plug>(MergetoolDiffExchangeUp)' : '<Up>'

" -- QuickFix
" make quickfix window modifiable
autocmd BufReadPost quickfix set modifiable

" --- Commands {{{
autocmd FileType python noremap <buffer> <Leader>fm :!python3 -m black -t py37 %<CR>
autocmd FileType terraform noremap <buffer> <Leader>fm :TerraformFmt<CR>
"  }}}

" --- Functions {{{

" Creates a second vertical split with the same buffer but scrolled one screen
" down. This allows to fit two screens of continouse text from the same file on
" a screen
fun! s:scroll()
    let l:save = &scrolloff

    set scrolloff=0 noscrollbind nowrap nofoldenable
    botright vsplit

    normal L
    normal j
    normal zt

    setlocal scrollbind
    exe "normal \<c-w>p"
    setlocal scrollbind

    let &scrolloff = l:save
endfun
command! Scroll call s:scroll()

" Temporary toggle showing of all main non-printable charaters, restoring the
" initial setting on next call. If there is no local override of 'listchars'
" for current window, it's the same as :set list!. However, for python I use
" :set list listchars=tab:→\  and in this case this might be handy.
let s:list_is_toggled = 0
let s:save_listchars = &listchars
let s:save_list = &list
fun! s:ListToggle()
    let s:list_is_toggled = !s:list_is_toggled

    if s:list_is_toggled
        let s:save_listchars = &l:listchars
        let s:save_list = &l:list
        setlocal listchars=tab:→\ ,eol:↵,trail:·,extends:↷,precedes:↶
        set list
    else
        let &l:listchars = s:save_listchars
        let &l:list = s:save_list
    endif
endfun
command! ListToggle call s:ListToggle()

"}}}
